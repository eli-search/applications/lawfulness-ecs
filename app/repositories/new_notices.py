import json
from typing import Any
from urllib import parse

from boto3_type_annotations.sqs import ServiceResource as SqsResource, Message
from pydantic import BaseModel

from config import NEW_NOTICES_CONFIG
from utils import logging

LOGGER = logging.logger(__name__)


class Notice(BaseModel):
    bucket: str
    key: str


class NewNoticeGroup(BaseModel):
    receipt_handle: str
    notices: list[Notice]


def get(sqs: SqsResource) -> list[NewNoticeGroup]:
    queue = sqs.Queue(NEW_NOTICES_CONFIG.queue_url)
    notices = [
        _parse_message(message)
        for message in queue.receive_messages(
            MessageAttributeNames=['All'],
            MaxNumberOfMessages=NEW_NOTICES_CONFIG.batch_size
        )
    ]
    LOGGER.debug(f"{len(notices)} new notices retrieved.")
    return notices


def remove(sqs: SqsResource, notices: list[NewNoticeGroup]):
    queue = sqs.Queue(NEW_NOTICES_CONFIG.queue_url)
    queue.delete_messages(Entries=[
        {'Id': str(i), 'ReceiptHandle': message.receipt_handle}
        for i, message in enumerate(notices)
    ])
    LOGGER.debug(f"{len(notices)} notices removed from queue")


def _parse_message(message: Message) -> NewNoticeGroup:
    return NewNoticeGroup(
        receipt_handle=message.receipt_handle,
        notices=list(filter(None, (
            _parse_notice(record)
            for record in json.loads(message.body).get("Records", [])
        )))
    )


def _parse_notice(record: dict[str, Any]) -> Notice | None:
    bucket = record.get("s3", {}).get("bucket", {}).get("name")
    key = record.get("s3", {}).get("object", {}).get("key")
    if not bucket or not key:
        return None
    return Notice(bucket=bucket, key=parse.unquote_plus(key))
