from boto3_type_annotations.s3 import ServiceResource as S3Resource

from utils import logging

LOGGER = logging.logger(__name__)


def get(s3: S3Resource, bucket: str, key: str) -> bytes:
    object_ = s3.Object(bucket, key)
    body = object_.get()['Body'].read()
    LOGGER.debug(f"Notices downloaded from s3://{bucket}/{key}.")
    return body
