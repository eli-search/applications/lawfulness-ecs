from botocore.config import Config

from config import AWS_CONFIG


def boto_config():
    config = {
        "config": Config(retries={
            "total_max_attempts": 3,
            "mode": "adaptive",
        }),
    }
    if AWS_CONFIG.endpoint_url:
        config["endpoint_url"] = AWS_CONFIG.endpoint_url
    return config
