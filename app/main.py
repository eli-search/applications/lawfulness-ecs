from multiprocessing import Pool
from pyexpat import ExpatError
from typing import Annotated

import boto3
import uvicorn
from boto3_type_annotations.dynamodb import ServiceResource as DynamoDBResource
from boto3_type_annotations.s3 import ServiceResource as S3Resource
from boto3_type_annotations.ssm import Client as SsmClient
from fastapi import FastAPI, File
from lxml.etree import XMLSyntaxError

from config import Mode, APP_CONFIG, AWS_CONFIG
from repositories import new_notices, xml_notices, flagged_notices
from repositories.flagged_notices import NoticeResult
from repositories.new_notices import Notice
from services import extraction, analysis, flagging
from services.extraction import TranslationModel
from services.parsing import Xml
from utils import aws, logging

LOGGER = logging.logger(__name__)


def run_notice_analysis_endpoint(notices_data: list[bytes],
                                 translation_model: TranslationModel) -> list[NoticeResult | None]:
    session = boto3.session.Session()
    ssm = session.client('ssm', region_name=AWS_CONFIG.region, **aws.boto_config())
    return [_process_notice(d, ssm, translation_model) for d in notices_data]


def run_task():
    LOGGER.info("Lawfulness task started.")
    with Pool(APP_CONFIG.thread_count) as p:
        p.map(_run_thread, range(0, APP_CONFIG.thread_count))
    LOGGER.info("No new notice retrieved from queue, task done.")


def _run_thread(thread_id: int):
    LOGGER.info(f"Thread {thread_id} started.")
    session = boto3.session.Session()
    sqs = session.resource('sqs', region_name=AWS_CONFIG.region, **aws.boto_config())
    s3 = session.resource('s3', region_name=AWS_CONFIG.region, **aws.boto_config())
    dynamodb = session.resource('dynamodb', region_name=AWS_CONFIG.region, **aws.boto_config())
    ssm = session.client('ssm', region_name=AWS_CONFIG.region, **aws.boto_config())
    translation_model = TranslationModel()
    while notice_groups := new_notices.get(sqs):
        for notice_group in notice_groups:
            for notice in notice_group.notices:
                _process_s3_notice(notice, dynamodb, s3, ssm, translation_model)
        new_notices.remove(sqs, notice_groups)
        LOGGER.debug(f"{len(notice_groups)} notices treated.")
    LOGGER.info(f"Thread {thread_id} done.")


def _process_s3_notice(notice: Notice, dynamodb: DynamoDBResource, s3: S3Resource, ssm: SsmClient,
                       translation_model: TranslationModel):
    try:
        notice_data = xml_notices.get(s3, notice.bucket, notice.key)
        result = _process_notice(notice_data, ssm, translation_model)
        if result:
            flagged_notices.save(dynamodb, result, notice)
    except:
        LOGGER.exception("Notice {%s} has failed", notice)


def _process_notice(notice_data: bytes, ssm: SsmClient, translation_model: TranslationModel) -> NoticeResult | None:
    try:
        xml = Xml(notice_data)
        notice_id = extraction.extract_notice_id(xml)
        if not notice_id:
            LOGGER.error(f"Notice ID not found")
            return None
        if not extraction.is_eforms_notice(xml):
            LOGGER.debug(f"Notice {notice_id} not supported because not eForms.")
            return None
        else:
            LOGGER.debug(f"Notice {notice_id} detected as eForms.")
        notice_fields = extraction.extract_fields(notice_id, xml, translation_model)
        notice_analysis = analysis.run_rules(ssm, notice_id, notice_fields)
        notice_flags = flagging.compute_flags(notice_id, notice_analysis)
        return NoticeResult(
            notice_id=notice_id,
            flags=notice_flags,
            field_analysis=notice_analysis,
            extracted_fields=notice_fields,
        )
    except XMLSyntaxError:
        LOGGER.exception("XML processing has failed")
        return None


if APP_CONFIG.mode == Mode.API:
    TRANSLATION_MODEL = TranslationModel()

    app = FastAPI()


    @app.get("/health")
    def health():
        pass


    @app.post("/notices/lawfulness")
    def analyze_notice_lawfulness(file: Annotated[list[bytes], File()]) -> list[NoticeResult | None]:
        return run_notice_analysis_endpoint(file, TRANSLATION_MODEL)


    uvicorn.run(app, host="0.0.0.0", port=8080)
else:
    run_task()
