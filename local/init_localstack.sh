#!/usr/bin/env bash

set -euo pipefail


send_notice_event() {
    filename=$1
    aws "$endpoint" sqs send-message \
      --queue-url "$queue_url" \
      --message-body '{"Records":[{"s3":{"bucket":{"name":"'"$bucket_name"'"},"object":{"key":"'"$filename"'"}}}]}' \
      --no-cli-pager
}

export $(cat local/.env | cut -d# -f1 | xargs)
bucket_name="d-ew1-ted-ai-input"
endpoint="--endpoint-url=$AWS_ENDPOINT_URL"

aws "$endpoint" dynamodb create-table \
  --table-name "$FLAGGED_NOTICES_DYNAMODB_TABLE_NAME" \
  --attribute-definitions "AttributeName=notice_id,AttributeType=S" \
  --key-schema "AttributeName=notice_id,KeyType=HASH" \
  --billing-mode "PAY_PER_REQUEST" \
  --no-cli-pager ||
  echo "Table already exists"

aws "$endpoint" s3api create-bucket \
  --bucket "$bucket_name" \
  --create-bucket-configuration "LocationConstraint=$AWS_REGION" \
  --no-cli-pager ||
  echo "Bucket already exists"

aws "$endpoint" ssm put-parameter \
  --name "$DB_PASSWORD_SSM_PARAMETER" \
  --value "$DB_LOCAL_PASSWORD" \
  --type "SecureString" \
  --no-cli-pager ||
  echo "SSM param for database password already exists"

aws "$endpoint" sqs create-queue --queue-name "$NEW_NOTICES_QUEUE_URL" --no-cli-pager
queue_url=$(aws "$endpoint" sqs get-queue-url \
  --queue-name "$NEW_NOTICES_QUEUE_URL"  \
  --query QueueUrl \
  --output text \
  --no-cli-pager)

aws "$endpoint" s3 cp "$(dirname "$0")"/resources/notices/ "s3://$bucket_name/" --recursive
for path in "$(dirname "$0")"/resources/notices/*; do
    send_notice_event "$(basename "$path")"
done
