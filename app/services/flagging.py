from pydantic import BaseModel

from services.analysis import NoticeFieldAnalysis
from utils import logging

LOGGER = logging.logger(__name__)


class NoticeFlags(BaseModel):
    has_test_word: bool
    has_sale_word: bool
    has_incoherent_text: bool
    is_forbidden_country: bool
    has_non_eu_country_and_not_funded_by_eu: bool
    is_contracting_body_private_and_not_funded_by_eu: bool
    has_not_whitelisted_international_contracting_body: bool

    def is_flagged(self) -> bool:
        return any(getattr(self, field_name) for field_name in NoticeFlags.model_fields.keys())


def compute_flags(notice_id: str, fields: NoticeFieldAnalysis) -> NoticeFlags:
    flags = NoticeFlags(
        has_test_word=fields.has_test_word,
        has_sale_word=fields.has_sale_word,
        has_incoherent_text=bool(fields.incoherent_texts),
        is_forbidden_country=fields.is_forbidden_country,
        has_non_eu_country_and_not_funded_by_eu=(bool(fields.non_eu_countries) and fields.is_not_funded_by_eu),
        is_contracting_body_private_and_not_funded_by_eu=(fields.is_contracting_body_private
                                                          and fields.is_not_funded_by_eu),
        has_not_whitelisted_international_contracting_body=(fields.is_contracting_body_international
                                                            and not fields.is_contracting_body_whitelisted),
    )
    LOGGER.debug(f"Computed flags for notice {notice_id}: {flags}")
    return flags
